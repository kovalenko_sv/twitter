window.onload = function () {
  document.body.classList.add("loaded_hiding");
  window.setTimeout(function () {
    document.body.classList.add("loaded");
    document.body.classList.remove("loaded_hiding");
  }, 500);
};
const root = document.querySelector("#root");
const div = document.createElement("div");

let userList = [];

class Card {
  async render() {
    await fetch("https://ajax.test-danit.com/api/json/users")
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        userList = data;
        return fetch("https://ajax.test-danit.com/api/json/posts");
      })
      .then((response) => {
        return response.json();
      })
      .then((post) => {
        post.forEach(({ title, body, userId, id }) => {
          div.dataset.id = id;

          userList.forEach(({ name, id, email }) => {
            if (id === userId) {
              const p = document.createElement("p");
              p.innerHTML = `<div
            style="border: 1px solid grey;
            border-radius:10px;
            max-width:50%; padding:10px">
          <p ><img src="./img/twitter.png" width="20" height="20">${email}</p>
          <p style="color:blue">${name}</p>
          <h2 style="text-transform:uppercase; font-size:17px">${title}</h2>
          <p>${body}</p>
          <button class="btn-delete"
          style="border-color:blue;cursor:pointer">
          Click to delete post
          </button>
          </div>`;
              root.append(p);
            }
          });
        });

        const btnDelete = [...document.getElementsByClassName("btn-delete")];
        btnDelete.forEach((el) => {
          el.addEventListener("click", deletePost);
        });
        function deletePost(e) {
          fetch(
            `https://ajax.test-danit.com/api/json/posts/${div.dataset.id}`,
            {
              method: "DELETE",
            }
          ).then(() => {
            e.target.parentNode.remove();
          });
        }
      });
  }
}
new Card().render();
